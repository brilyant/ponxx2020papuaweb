import React, { Component } from 'react';
import {
  Col,
  Row, 
} from 'reactstrap';
import axios from 'axios'

import TableMenuDashboardMobApp from '../../../components/TableMenuDashboardMobApp'
import CRUDMenuItem from '../../../components/CRUDMenuItem'
import StaticVar from '../../../StaticVar';

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      iscreate: false,
      item: null,
      deleting: false
    };
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
          {
            this.state.iscreate ? 
              <CRUDMenuItem 
                processSuccess={() => this.setState({ iscreate: false, item: undefined })} 
                cancelClicked={() => this.setState({ iscreate: false, item: undefined })} 
                item={this.state.item}
              />  
              : <TableMenuDashboardMobApp 
                  createClicked={() => this.setState({ iscreate: true })}
                  editRow={(x) => this.setState({ iscreate: true, item: x })}
                  isdeleting={yes => {
                    this.setState({ deleting: yes })
                  }}
                  // deleteRow={(x) => this.deleteProcess(x.id)}
                />
          } 
          </Col> 
        </Row>
      </div>
    );
  }
}

export default Forms;
