import React, { Component } from 'react';
import {  
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Form,
    FormGroup,
    Badge,
    Input,
    Label, 
} from 'reactstrap';
import uuidv4 from 'uuid/v4'
import { css } from '@emotion/core';
import ClipLoader from 'react-spinners/ClipLoader';

import axios from 'axios'
import StaticVar from '../StaticVar'

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

class CRUDMenuItem extends Component {
  constructor(props) {
    super(props); 
    this.state = {  
        loading: false,
        // processing: false,
        form: {
          name: null,
          background: null,
          icon: null,
          url: null,
          no: null,
          _id: null
        }
    };
  }

  componentDidMount() {
    if (this.props.item) {
      this.setState({
        form: this.props.item
      })
    }else {
      //generate id manual
      this.setState({ 
        form: {...this.state.form, _id: uuidv4()} 
      })
    } 
  } 

  proccessData() {     
      console.log(this.state.form);
      let validated = true
      Object.keys(this.state.form).forEach(key=>{
        // console.log(`${key} : ${obj[key]}`);
        if (this.state.form[key] === null) {
            validated = false
        } 
     });

    if (!validated) { 
        alert('Parameter belum lengkap !')
        return
    }

    this.setState({ loading: true })
    axios.put(`${StaticVar.HOST_URL}/upsert`, { form: this.state.form })
    .then(response => { 
        alert(response.data) 
        this.setState({ loading: false }, ()=> this.props.processSuccess())
 
    })
    .catch((error) => {
      // handle error
      this.setState({ loading: false })
      console.log(error);
      alert(error)
    }) 
  }

  render() {
      if (this.state.processing) {
        return (
            <div>
              <ClipLoader
                css={override}
                size={150} // or 150px
                color={'#123abc'}
                loading={this.state.loading}
              />
            </div> 
        )
      }
    return (
      <div className="animated fadeIn">
            <Card>
              <CardHeader>
                <strong>Buat Menu</strong> Item
              </CardHeader>
              <CardBody>
                <Form action="" method="post" className="form-horizontal">
                    <FormGroup>
                        <Label htmlFor="name">Nama</Label>
                        <Input value={this.state.form.name} type="text" id="name" placeholder="Nama Menu" required onChange={e => this.setState({ form: {...this.state.form, name: e.target.value} }) } /> 
                    </FormGroup>
                    <FormGroup>
                        <Label htmlFor="background">Background Color</Label>
                        <Input value={this.state.form.background} type="text" id="background" placeholder="#FFFFF" required onChange={e => this.setState({ form: {...this.state.form, background: e.target.value} }) } /> 
                    </FormGroup>

                    <FormGroup>
                        <Label htmlFor="icon">Icon</Label>
                        <Input value={this.state.form.icon} type="text" id="icon" placeholder="Icon" required onChange={e => this.setState({ form: {...this.state.form, icon: e.target.value} }) } /> 
                    </FormGroup>

                    <FormGroup>
                        <Label htmlFor="url">Url</Label>
                        <Input value={this.state.form.url} type="url" id="url" placeholder="Url" required onChange={e => this.setState({ form: {...this.state.form, url: e.target.value} }) } /> 
                    </FormGroup>

                    <FormGroup>
                        <Label htmlFor="no">No Urut</Label>
                        <Input value={this.state.form.no} type="number" id="no" placeholder="No Urut" required onChange={e => this.setState({ form: {...this.state.form, no: e.target.value} }) } /> 
                    </FormGroup>
                    
                </Form>
              </CardBody>
              <CardFooter>
                <Button onClick={() => this.proccessData()} size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Proses</Button>
                <Button onClick={this.props.cancelClicked} type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Batal</Button>
              </CardFooter>
            </Card>
      </div>
    );
  }
}

export default CRUDMenuItem;
