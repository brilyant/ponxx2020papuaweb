import React, { Component } from 'react';
import { 
  Button,
  Card,
  CardBody, 
  CardHeader, 
  Table
} from 'reactstrap';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

import axios from 'axios'
import StaticVar from '../StaticVar'

const createOptions = (_id) => ({
  title: 'Hapus Data',
  message: 'Yakin Hapus Data ?',
  buttons: [
    {
      label: 'Yes',
      onClick: () => {
        this.props.isdeleting(true)
        axios.delete(`${StaticVar.HOST_URL}/delete/${_id}`)
        .then(response => { 
          // const id = response.data
          if (response.status === 200) {
            this.setState({ datatable: this.state.datatable.filter( x => x._id !== _id) }, () => this.props.isdeleting(false))
          }
            
        })
        .catch((error) => {
          // handle error
          this.props.isdeleting(false)
          console.log(error);
          alert(error)
        }) 
      }
    },
    {
      label: 'No',
      onClick: () => {}
    }
  ],
  // childrenElement: () => <div />,
  // customUI: ({ onClose }) => <div>Custom UI</div>,
  // closeOnEscape: true,
  // closeOnClickOutside: true,
  // willUnmount: () => {},
  // onClickOutside: () => {},
  // onKeypressEscape: () => {}
});

class TableMenuDashboardMobApp extends Component {
  constructor(props) {
    super(props); 
    this.state = { 
        datatable: [],
        loading: false,
    };
  }

  componentDidMount() {
    this.setState({ loading: true })
  axios.get(`${StaticVar.HOST_URL}/getmenu`)
  .then(response => {  
      
    this.setState({ datatable: response.data, loading: false })
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  }) 
}



deleteProcess({ _id }) {
  console.log('_id', _id);
  
  // const option = createOptions.bind(this,_id)
  confirmAlert({
    title: 'Hapus Data',
    message: 'Yakin Hapus Data ?',
    buttons: [
      {
        label: 'Yes',
        onClick: () => {
          this.props.isdeleting(true)
          axios.delete(`${StaticVar.HOST_URL}/delete/${_id}`)
          .then(response => { 
            // const id = response.data
            if (response.status === 200) {
              this.setState({ datatable: this.state.datatable.filter( x => x._id !== _id) }, () => this.props.isdeleting(false))
            }
              
          })
          .catch((error) => {
            // handle error
            this.props.isdeleting(false)
            console.log(error);
            alert(error)
          }) 
        }
      },
      {
        label: 'No',
        onClick: () => {}
      }
    ]})
  
}

_renderAction(x) {
  return(
    <div style={{ flex:1, flexDirection: 'column', justifyContent: 'center', alignContent: 'center' }}>
      <Button onClick={() => this.props.editRow(x)} size="sm" color="primary" style={{ margin: 3 }}>Edit</Button>
      <Button onClick={() => this.deleteProcess(x)} size="sm" color="danger" style={{ margin: 3 }}>Hapus</Button>
    </div>
  )
}

  render() {
    return (
      <div className="animated fadeIn"> 
          <Card> 
              <CardHeader>
                <i className="fa fa-align-justify"></i> Table Menu MobApp
              </CardHeader>
              <CardBody> 
                <Button onClick={this.props.createClicked} size="sm" color="primary" style={{ marginBottom: 15 }}>Buat Menu</Button>
                <Table hover bordered striped responsive size="sm">
                  <thead>
                  <tr>
                    <th>Nama Menu</th>
                    <th>Background Color (Hex)</th>
                    <th>Icon</th>
                    <th>WebView URL</th> 
                    <th>No Urut</th> 
                    <th> </th> 
                  </tr>
                  </thead>
                  <tbody>
                      {
                          this.state.datatable.map(x => {
                            return (
                                <tr key={x._id}>
                                    <td>{(x.name ? x.name : '')}</td>
                                    <td>{(x.background ? x.background : '')}</td>
                                    <td>{(x.icon ? x.icon : '')}</td>
                                    <td>{(x.url ? x.url : '')}</td>
                                    <td>{(x.no ? x.no : '')}</td> 
                                    <td>{this._renderAction(x)}</td> 
                                </tr>
                            )
                        })
                      }
                  
                  
                  </tbody>
                </Table>
              </CardBody>
            </Card> 
      </div>
    );
  }
}

export default TableMenuDashboardMobApp;
